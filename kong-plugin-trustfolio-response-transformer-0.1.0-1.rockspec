package = "kong-plugin-trustfolio-response-transformer"  --
                                  -- as a convention; stick to the prefix: `kong-plugin-`
version = "0.1.0-1"               --
-- The version '0.1.0' is the source code version, the trailing '1' is the version of this rockspec.
-- whenever the source version changes, the rockspec should be reset to 1. The rockspec version is only
-- updated (incremented) when this file changes, but the source remains the same.

--
-- Here we extract it from the package name.
local pluginName = package:match("^kong%-plugin%-(.+)$")  -- "trustfolio-response-transformer"

supported_platforms = {"linux", "macosx"}
source = {
  url = "https://edgarrroussille@bitbucket.org/edgarrroussille/trustfolio-response-transformer.git",
  tag = "0.1.0"
}

description = {
  summary = "Custom plugin for Kong to allow for nested json transformation of a response.",
  homepage = "http://trustfolio.co",
  license = "Apache 2.0"
}

dependencies = {
}

build = {
  type = "builtin",
  modules = {
    --
    ["kong.plugins."..pluginName..".handler"] = "kong/plugins/"..pluginName.."/handler.lua",
    ["kong.plugins."..pluginName..".schema"] = "kong/plugins/"..pluginName.."/schema.lua",
    ["kong.plugins."..pluginName..".header_filter"] = "kong/plugins/"..pluginName.."/body_transformer.lua",
    ["kong.plugins."..pluginName..".header_filter"] = "kong/plugins/"..pluginName.."/header_transformer.lua",
  }
}
